# limesurvey

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for deploying limesurvey

## Source Code

* <https://gitlab.com/pleio/helm-charts/limesurvey-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| config.database.host | string | `nil` |  |
| config.database.name | string | `nil` |  |
| config.database.password | string | `nil` |  |
| config.database.user | string | `nil` |  |
| config.email.domain | string | `nil` |  |
| config.email.host | string | `nil` |  |
| config.email.hostname | string | `nil` |  |
| config.email.user | string | `nil` |  |
| config.nonce | string | `nil` |  |
| config.secretKey | string | `nil` |  |
| domain | string | `nil` |  |
| existingSecret | string | `nil` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"registry.gitlab.com/pleio/limesurvey"` |  |
| image.tag | string | `"latest"` |  |
| ingress.annotations."cert-manager.io/cluster-issuer" | string | `"letsencrypt-pleio-prod-issuer"` |  |
| ingress.annotations."kubernetes.io/ingress.class" | string | `"nginx"` |  |
| replicaCount | int | `1` |  |
| storage.className | string | `"efs-v2"` |  |
| storage.name | string | `nil` |  |
| storage.storageSize | string | `"20Gi"` |  |
