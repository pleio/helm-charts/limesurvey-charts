{{/*
Expand the name of the chart.
*/}}
{{- define "limesurvey.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{- define "limesurvey.initName" -}}
{{- printf "%s-init" ((include "limesurvey.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "limesurvey.secretsName" -}}
{{- printf "%s-secrets" ((include "limesurvey.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "limesurvey.tlsSecretName" -}}
{{- printf "tls-%s" ((include "limesurvey.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "limesurvey.storageName" -}}
{{- if .Values.storage.existingDataStorage }}
{{- .Values.storage.existingDataStorage -}}
{{- else }}
{{- printf "%s-data" ((include "limesurvey.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "limesurvey.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "limesurvey.labels" -}}
helm.sh/chart: {{ include "limesurvey.chart" . }}
{{ include "limesurvey.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "limesurvey.selectorLabels" -}}
app.kubernetes.io/name: {{ include "limesurvey.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
